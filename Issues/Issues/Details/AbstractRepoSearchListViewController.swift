/*
* Copyright (c) 2015 Razeware LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

import UIKit
import IssuesCore

class AbstractRepoSearchListViewController: UITableViewController {
  // Details
  var searchController: UISearchController = UISearchController(searchResultsController: nil)
  var searchTypingTimer: NSTimer?
  var currentSearchRepoAction: SearchReposActionType?

  override func viewDidLoad() {
    super.viewDidLoad()
    configureSearchController()
    addSearchBar()
  }

  func configureSearchController() {
    searchController.searchResultsUpdater = self
    searchController.delegate = self
    searchController.hidesNavigationBarDuringPresentation = true
    searchController.dimsBackgroundDuringPresentation = false
    searchController.obscuresBackgroundDuringPresentation = false
    searchController.searchBar.sizeToFit()
    searchController.searchBar.tintColor = view.tintColor
  }

  func addSearchBar() {
    tableView.tableHeaderView = searchController.searchBar
  }

  func dismissOnSelection() {
    var completion = { }
    if searchController.active {
      completion = {
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
      }
    }
    dismissViewControllerAnimated(true, completion: completion)
  }

  func startNewSearchWithText(searchText: String) -> SearchReposActionType {
    fatalError("\(__FUNCTION__) is an abstract method, this method must be implemented in a subclass.")
  }
}

extension AbstractRepoSearchListViewController: UISearchResultsUpdating {
  func updateSearchResultsForSearchController(searchController: UISearchController) {
    guard searchController.active else { return }

    let searchText = searchController.searchBar.text!
    guard searchText.characters.isEmpty == false else {
      return
    }

    searchTypingTimer?.invalidate()
    searchTypingTimer = NSTimer.scheduledTimerWithTimeInterval(0.6, target: self, selector: "startNewSearchFromTimer:",
      userInfo: ["searchText": searchText], repeats: false)
  }

  func startNewSearchFromTimer(timer: NSTimer) {
    if let currentAction = currentSearchRepoAction {
      currentAction.cancel()
    }

    guard let searchText = timer.userInfo?["searchText"] as? String else { return }
    currentSearchRepoAction = startNewSearchWithText(searchText)
  }
}

extension AbstractRepoSearchListViewController: UISearchControllerDelegate {
  func willDismissSearchController(searchController: UISearchController) {
    if let currentAction = currentSearchRepoAction {
      currentAction.cancel()
    }
  }
}
