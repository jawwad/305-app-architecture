/*
 * Copyright (c) 2015 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit
import IssuesCore

class FavoriteRepoListViewController: UITableViewController {
  var repos: [Repo] = []
  var localDataStore: RepoLocalDataStore!
  var didSelectRepo: (Repo -> Void)?

  // MARK: Get repos from data store on will appear

  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    localDataStore.getFavoriteRepos { repos, error in
      self.onGotFavoriteRepos(repos, error: error)
    }
  }

  func onGotFavoriteRepos(repos: [Repo]?, error: ErrorType?) {
    if let _ = error { return }
    if let repos = repos {
      onMainQueue {
        self.repos = repos
        self.tableView.reloadData()
      }
    }
  }
}

// MARK: Lab: Navigate to selected favorite repo

extension FavoriteRepoListViewController {
  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let repo = repos[indexPath.row]
    didSelectRepo?(repo)
  }
}

// MARK: Details

extension FavoriteRepoListViewController {
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return repos.count
  }

  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
    cell.textLabel!.text = repos[indexPath.row].name
    return cell
  }
}
