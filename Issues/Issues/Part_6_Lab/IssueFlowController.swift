/*
* Copyright (c) 2015 Razeware LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

import UIKit
import IssuesCore

class IssueFlowController: UINavigationController {

  override func viewDidLoad() {
    super.viewDidLoad()
    let favoriteReposViewController = createFavoriteReposViewController()
    viewControllers = [favoriteReposViewController]
  }

  func createFavoriteReposViewController() -> FavoriteRepoListViewController {
    let vc = instantiateFavoriteReposViewController()
    vc.didSelectRepo = { [weak self] repo in
      guard let strongSelf = self else { return }
      strongSelf.pushViewController(strongSelf.createRepoDetailViewControllerWithRepo(repo), animated: true)
    }
    return vc
  }

  func createRepoDetailViewControllerWithRepo(repo: Repo) -> RepoDetailViewController {
    let vc = instantiateRepoDetailViewController()
    vc.repo = repo
    vc.didSelectIssuesForRepo = { [weak self] repo in
      guard let strongSelf = self else { return }
      strongSelf.pushViewController(strongSelf.createIssueListViewControllerWithRepo(repo), animated: true)
    }
    return vc
  }

  func createIssueListViewControllerWithRepo(repo: Repo) -> IssueListViewController {
    let vc = instantiateIssueListViewController()
    vc.repo = repo
    vc.didSelectIssue = { [weak self] issue in
      guard let strongSelf = self else { return }
      strongSelf.pushViewController(strongSelf.createIssueDetailViewControllerWithIssue(issue), animated: true)
    }
    return vc
  }

  func createIssueDetailViewControllerWithIssue(issue: Issue) -> IssueDetailViewController {
    let vc = instantiateIssueDetailViewController()
    vc.issue = issue
    return vc
  }
}

extension IssueFlowController {
  func instantiateFavoriteReposViewController() -> FavoriteRepoListViewController {
    return storyboard!.instantiateViewControllerWithIdentifier("FavoriteRepos") as! FavoriteRepoListViewController
  }

  func instantiateRepoDetailViewController() -> RepoDetailViewController {
    return storyboard!.instantiateViewControllerWithIdentifier("RepoDetail") as! RepoDetailViewController
  }

  func instantiateIssueListViewController() -> IssueListViewController {
    return storyboard!.instantiateViewControllerWithIdentifier("IssueList") as! IssueListViewController
  }

  func instantiateIssueDetailViewController() -> IssueDetailViewController {
    return storyboard!.instantiateViewControllerWithIdentifier("IssueDetail") as! IssueDetailViewController
  }
}
