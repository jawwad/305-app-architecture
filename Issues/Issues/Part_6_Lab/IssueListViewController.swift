/*
 * Copyright (c) 2015 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit
import IssuesCore

class IssueListViewController: UITableViewController {
  var repo: Repo!
  var getRepoIssuesActionFactory: ((Repo, IssueListOutcome -> Void) -> Action)!
  var issues: [Issue] = []
  var didSelectIssue: (Issue -> Void)?

  override func viewDidLoad() {
    super.viewDidLoad()

    let action = getRepoIssuesActionFactory(repo) { outcome in
      onMainQueue {
        hideNetworkActivity()
        switch outcome {
        case .Success(let issues):
          self.issues = issues
          self.tableView.reloadData()
        case .Cancelled:
          break
        case .Error(let error):
          print(error)
        }
      }
    }
    action.start()
    showNetworkActivity()
  }

  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return issues.count
  }

  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
    cell.textLabel!.text = issues[indexPath.row].title
    return cell
  }

  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let issue = issues[indexPath.row]
    didSelectIssue?(issue)
  }
}
