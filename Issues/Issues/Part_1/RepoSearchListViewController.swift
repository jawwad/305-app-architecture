/*
 * Copyright (c) 2015 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit
import IssuesCore

typealias SearchReposActionType = Action

class RepoSearchListViewController: AbstractRepoSearchListViewController {
  var matchingRepos: [Repo] = []
  var localDataStore: RepoLocalDataStore!
  var searchActionFactory:((String, (RepoListOutcome -> Void)) -> Action)!

  override func startNewSearchWithText(searchText: String) -> SearchReposActionType {
    let searchAction = searchActionFactory(searchText) { [weak self] outcome in
      self?.onSearchCompleteWithOutcome(outcome)
    }
    searchAction.start()
    showNetworkActivity()
    return searchAction
  }
}

// MARK: Perform repo search and process results

extension RepoSearchListViewController {
  func onSearchCompleteWithOutcome(outcome: RepoListOutcome) {
    hideNetworkActivity()
    switch outcome {
    case .Success(let matchingRepos):
      onMainQueue {
        self.matchingRepos = matchingRepos
        self.tableView.reloadData()
      }
    case .Cancelled:
      break
    case .Error(let error):
      print(error)
    }
  }
}

// MARK: Save selected repo

extension RepoSearchListViewController {
  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let repo = matchingRepos[indexPath.row]
    localDataStore.addFavoriteRepo(repo) { error in
      self.onSaveRepoWithError(error)
    }
  }

  func onSaveRepoWithError(error: ErrorType?) {
    if let _ = error { return }
    onMainQueue {
      self.dismissOnSelection()
    }
  }
}

// MARK: Table View Details

extension RepoSearchListViewController {
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return matchingRepos.count
  }

  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
    let repo = matchingRepos[indexPath.row]
    cell.textLabel?.text = repo.identifier
    return cell
  }
}
