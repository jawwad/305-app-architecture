/*
 * Copyright (c) 2015 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import Swinject
import IssuesCore

extension SwinjectStoryboard {
  class func setup() {
    let diContainer = defaultContainer

    diContainer.register(RepoLocalDataStore.self) { resolvable in
      return RepoUserPreferenceLocalDataStore()
    }.inObjectScope(.Container)

    diContainer.register(SearchReposAction.self) { _, searchText, onComplete in
      return GitHubSearchReposAction(searchText: searchText, onComplete: onComplete)
    }.inObjectScope(.Graph)

    diContainer.registerForStoryboard(FavoriteRepoListViewController.self) { r, vc in
      vc.localDataStore = r.resolve(RepoLocalDataStore.self)!
    }

    diContainer.registerForStoryboard(RepoSearchListViewController.self) { r, vc in
      vc.localDataStore = r.resolve(RepoLocalDataStore.self)!
      vc.searchActionFactory = { searchText, onComplete in
        return r.resolve(SearchReposAction.self, arguments: (searchText, onComplete))!
      }
    }

    registerDependenciesForLabWithContainer(diContainer)
  }
}


// MARK: Helper functions

private func registerDependenciesForLabWithContainer(diContainer: Container) {
  diContainer.register(GetRepoIssuesAction.self) { _, repo, onComplete in
    return GitHubGetRepoIssuesAction(repo: repo, onComplete: onComplete)
    }.inObjectScope(.Graph)

  diContainer.registerForStoryboard(IssueListViewController.self) { r, vc in
    vc.getRepoIssuesActionFactory = { repo, onComplete in
      r.resolve(GetRepoIssuesAction.self, arguments: (repo, onComplete))!
    }
  }
}
