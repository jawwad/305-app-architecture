/*
 * Copyright (c) 2015 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import Foundation
import OctoKit

class OCTIssueDetailed: OCTIssue {
  @objc var number: Int = 0
  @objc var state: String = ""
  @objc var body: String = ""
  @objc var dateCreated: NSDate? = nil
  @objc var dateUpdated: NSDate? = nil
  @objc var dateClosed: NSDate? = nil
  @objc var user: OCTUser? = nil
  @objc var assignee: OCTUser? = nil

  @objc override class func JSONKeyPathsByPropertyKey() -> [NSObject: AnyObject] {
    let keyPaths = super.JSONKeyPathsByPropertyKey() as NSDictionary
    return keyPaths.mtl_dictionaryByAddingEntriesFromDictionary(["dateCreated": "created_at",
      "dateUpdated": "updated_at",
      "dateClosed": "closed_at"])
  }

  @objc class func dateCreatedJSONTransformer() -> NSValueTransformer {
    return NSValueTransformer(forName: OCTDateValueTransformerName)!
  }

  @objc class func dateUpdatedJSONTransformer() -> NSValueTransformer {
    return NSValueTransformer(forName: OCTDateValueTransformerName)!
  }

  @objc class func dateClosedJSONTransformer() -> NSValueTransformer {
    return NSValueTransformer(forName: OCTDateValueTransformerName)!
  }

  @objc class func userJSONTransformer() -> NSValueTransformer {
    return NSValueTransformer.mtl_JSONDictionaryTransformerWithModelClass(OCTUser.self)
  }

  @objc class func assigneeJSONTransformer() -> NSValueTransformer {
    return NSValueTransformer.mtl_JSONDictionaryTransformerWithModelClass(OCTUser.self)
  }
}
