/*
* Copyright (c) 2015 Razeware LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

import AsyncOperation
import OctoKit

public typealias RepoListOutcome = Outcome<[Repo]>

public class GitHubSearchReposAction: SearchReposAction {
  var disposable: RACDisposable?

  override public func run() {
    let client = OCTClient()
    let signal = client.searchRepositoriesWithQuery(searchText, orderBy: "star", ascending: true)
    disposable = signal.subscribeNext {
      result in self.processResult(result)
    }
  }

  func processResult(result: AnyObject) {
    let matchingGitHubRepos = resultToRepositories(result)
    let repos = matchingGitHubRepos.mapToGenericRepos()
    finishedExecutingOperationWithOutcome(.Success(repos), onComplete: onComplete)
  }

  override public func cancel() {
    super.cancel()
    disposable?.dispose()
    onComplete(.Cancelled)
  }

}


// MARK: Helper functions

extension CollectionType where Generator.Element == OCTRepository {
  func mapToGenericRepos() -> [Repo] {
    return map { $0.toRepo() }
  }
}

// TODO: Add documentation advising to not force cast for real production code.
private func resultToRepositories(result: AnyObject) -> [OCTRepository] {
  return (result as! OCTRepositoriesSearchResult).repositories as! [OCTRepository]
}

extension OCTRepository {
  func toRepo() -> Repo {
    return Repo(name: name, owner: ownerLogin, identifier: "\(ownerLogin)/\(name)")
  }
}
