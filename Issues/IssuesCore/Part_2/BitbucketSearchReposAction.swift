/*
* Copyright (c) 2015 Razeware LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

import Foundation
import AsyncOperation

public class BitbucketSearchReposAction: SearchReposAction {
  var dataTask: NSURLSessionDataTask?

  override public func run() {
    // Create GET URL request for repo search
    guard let request = try? NSMutableURLRequest(searchText: searchText) else {
      finishedExecutingOperationWithError()
      return
    }
    // Create data task and run
    let session = NSURLSession.sharedSession()
    let task = session.dataTaskWithRequest(request) { [weak self] dataOrNil, responseOrNil, errorOrNil in
      guard let strongSelf = self else { return }
      strongSelf.processResult(dataOrNil: dataOrNil, responseOrNil: responseOrNil, errorOrNil: errorOrNil)
    }
    task.resume()
    dataTask = task
  }

  func processResult(dataOrNil dataOrNil: NSData?, responseOrNil: NSURLResponse?, errorOrNil: NSError?) {
    // Finish with error if data task completes with error
    if let _ = errorOrNil {
      finishedExecutingOperationWithError()
      return
    }
    if let data = dataOrNil, response = responseOrNil as? NSHTTPURLResponse {
      // Finish with error if server responds with error HTTP status code
      guard response.statusCode.isSuccessHTTPStatuCode else {
        finishedExecutingOperationWithError()
        return
      }
      // Finish with error if cannot deserialize JSON into repo list
      guard let repos = try? data.toRepoList() else {
        finishedExecutingOperationWithError()
        return
      }
      // All is good, finish with success
      finishedExecutingOperationWithOutcome(.Success(repos), onComplete: onComplete)
      return
    }
    finishedExecutingOperationWithError()
  }

  func finishedExecutingOperationWithError() {
    finishedExecutingOperationWithOutcome(.Error(BitbucketError.RepoSearchFailedError), onComplete: onComplete)
  }

  override public func cancel() {
    super.cancel()
    dataTask?.cancel()
    onComplete(.Cancelled)
  }
}


// MARK: Helper extensions

extension NSMutableURLRequest {
  private convenience init(searchText: String) throws {
    guard let urlEncodedSearchText = searchText.searchTermQueryParameterString() else {
      throw IssuesCoreError.URLStringEncodingError
    }
    guard let url = NSURL(string: "https://bitbucket.org/xhr/repos?term=\(urlEncodedSearchText)") else {
      throw IssuesCoreError.URLInitError
    }
    self.init(URL: url)
    setValue("nocheck", forHTTPHeaderField: "X-Atlassian-Token")
  }
}

extension String {
  private func searchTermQueryParameterString() -> String? {
    return stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())
  }
}

extension Int {
  private var isSuccessHTTPStatuCode: Bool {
    return 200...299 ~= self
  }
}
