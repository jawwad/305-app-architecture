/*
* Copyright (c) 2015 Razeware LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

import Foundation

public class RepoUserPreferenceLocalDataStore: RepoLocalDataStore {
  let favoriteReposUserDefaultsKey = "FavoriteRepos"
  let mutationQueue = dispatch_queue_create("com.razeware.issuescore.RepoUserPreferenceLocalDataStore", DISPATCH_QUEUE_SERIAL)
  let defaults = NSUserDefaults.standardUserDefaults()

  public init() {}

  public func addFavoriteRepo(repo: Repo, onComplete: ErrorType? -> Void) {
    getFavoriteRepos { reposOrNil, errorOrNil in
      if let _ = errorOrNil {
        onComplete(RepoLocalStoreError.UserDefaultsStoreSaveError)
        return
      }

      guard let repos = reposOrNil else {
        onComplete(RepoLocalStoreError.UserDefaultsStoreSaveError)
        return
      }

      var reposSet = Set(repos)
      reposSet.insert(repo)

      let repoDictionaries = reposSet.map { repo in
        return ["name": repo.name, "owner": repo.owner, "full_slug": repo.identifier]
      }

      self.defaults.setObject(repoDictionaries, forKey: self.favoriteReposUserDefaultsKey)
      onComplete(nil)
    }
  }

  public func getFavoriteRepos(onComplete onComplete: ([Repo]?, ErrorType?) -> Void) {
    onMutationQueue {
      if let defaultsObject = self.defaults.objectForKey(self.favoriteReposUserDefaultsKey) {
        guard let repoDictionaryArray = defaultsObject as? [[NSObject: AnyObject]] else {
          onComplete(nil, RepoLocalStoreError.UserDefaultsStoreFetchError)
          return
        }
        guard let repos = try? repoDictionaryArray.toRepoList() else {
          onComplete(nil, RepoLocalStoreError.UserDefaultsStoreFetchError)
          return
        }
        onComplete(repos, nil)
      } else {
        onComplete([], nil)
      }
    }
  }

  private func onMutationQueue(mutation: Void -> Void) {
    dispatch_async(mutationQueue) {
      mutation()
    }
  }
}
