/*
 * Copyright (c) 2015 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import AsyncOperation
import OctoKit

public class GitHubGetRepoIssuesAction: GetRepoIssuesAction {
  var disposable: RACDisposable?

  override public func run() {
    let client = OCTClient()
    let repoDictionary = ["name": repo.name, "ownerLogin": repo.owner]
    guard let gitHubRepository = try? OCTRepository(dictionary: repoDictionary, error:()) else {
      finishedExecutingOperationWithOutcome(.Error(GitHubError.GetIssuesError),
        onComplete: onComplete)
      return
    }
    let signal = client.fetchRepositoryOpenIssues(gitHubRepository)
    disposable = signal.collect().subscribeNext { result in
      if let gitHubIssues = result as? [OCTIssueDetailed] {
        let issues = gitHubIssues.map { issue in
          return Issue(number: "\(issue.number)", title: issue.title, body: issue.body)
        }
        self.finishedExecutingOperationWithOutcome(.Success(issues), onComplete: self.onComplete)
      }
    }
  }
}
